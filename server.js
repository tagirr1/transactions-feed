'use strict';

const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const generateName = require('sillyname');
const FREQUENCY = 3000
const PROJECTS_COUNT = 30
const USERS_COUNT = 60
const THEMES_COUNT = 4
const NAME = 'name'; const AGE = 'age'; const GENDER = 'gender'; const COUNTRY ='country'
const countriesList = ['RUS','USA','FIN','EST','LTU','UKR','BLR','JPN','MEX','DEU']

function createProjects(){
  let i = 0
  let projects = []
  while (i < PROJECTS_COUNT) {
    projects.push(generateName())
    i++;
  }
  return projects
}

class User {
  constructor (id) {
    this.id = id
    this[NAME]= generateName()
    this[AGE] = randBetweenMinMax(10, 80)
    this[GENDER] = randomItemFromArr(['male','female'])
    this[COUNTRY] = randomItemFromArr(countriesList)
  }
}

function createUsers(){
  let i = 0
  let users=[]
  while (i < USERS_COUNT) {
    users.push(new User(i))
    i++;
  }
  return users
}

// function for create a random transaction
function randomItemFromArr (arr) {
  return arr[Math.floor(Math.random()*arr.length)]
}

function randBetweenMinMax (min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1)
  rand = Math.round(rand);
  return rand;
}

// transaction constructor
function createDonate(projects, users) {
  let id=0;
  return function() {
   id++
   let themeId = randBetweenMinMax(0, (THEMES_COUNT-1))
   let userId = randBetweenMinMax(0, (USERS_COUNT-1))

   return {
       id : id,
       theme_id : themeId,
       user_id : userId,
       project_name : randomItemFromArr(projects),
       amount : randBetweenMinMax(0, 200),
       user_country: users[userId][COUNTRY],
       user_gender: users[userId][GENDER],
       user_age: users[userId][AGE],
       user_name: users[userId][NAME],
       created : (new Date())
     }
  }
}

function startDonating(frequency, projects, users){ //ms for repeat donation message

  let newDonate = createDonate(projects, users);
  let RepeatTransaction = setInterval(
    function(){
      let donation = newDonate()
      io.to('donations').emit('new', donation)
    }, frequency
  )
}

//listening messages from clients
server.listen(3000);

io.on('connection', function (socket) {
  socket.join('donations'); //new listener will be added to main channel
});

startDonating(FREQUENCY, createProjects(), createUsers())
