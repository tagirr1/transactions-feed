
nodemon server.js // запуск сервера

cd client ; npm run dev  // запуск клиента


Предположим у нас есть система доната денег на разные проекты. Каждый проект относится к определенной тематике. Пользователь может перевести какую-то сумму в проект.
Все транзакции принимаются страницей, показывающей агрегированную информацию.
Так же у пользователя есть параметры возраст, пол, страна.

сообщения такого вида:
{
theme_id : id_int,
user_id : id_int,
project_name : name_string
amount : 100.56 ($)
user_country: name_string
user_gender: gender
user_age: age_int
created : timestamp
}

Нужно написать
1. генератор таких рандомных транзакций, при старте задается частота появления транзакций (в милисекундах), created у транзакций - текущее время. сгенерированные платежи принимаются обрабатывающей страницей.

2. страничку, которая в реальном времени выводит сводную информацию по всем тематикам, на которые есть платежи и отдельно по каждому проекту внутри тематики.


3. Отображение реалтайм фида транзакций на этой странице.

Дополнительно (необязательно, но будет плюсом):

4. Написать добавление на страницу графиков-виджетов (в виде баров). По оси х пользователь выбирает одно из значений: пол, возраст, страна, проект, тематика. По оси у: сумма, кол-во платежей, кол-во заплативших юзеров.

5. Написать генератор транзакций на nodejs (или любом другом языке для разработки серверной части). Отправлять транзакции на клиент с помощью websocket.
