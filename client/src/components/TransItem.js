import React, { Component } from 'react'
import PropTypes from 'prop-types';

export default class TransItem extends Component {
  render() {
    const { id, info } = this.props
    return <div className=''>
      <p>New transfer id:{id} to a project {info.project_name}</p>
    </div>
  }
}

TransItem.propTypes = {
  id: PropTypes.number.isRequired,
  info: PropTypes.object.isRequired
}
