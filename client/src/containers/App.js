import React, { Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import TransactionsFeed from '../components/TransactionsFeed'
import Themes from '../components/Themes'


// import * as Actions from '../actions/Actions'
import {newTransaction} from '../actions/TransactionsActions'
import {socket} from '../socket.js'




class App extends Component {

  componentWillMount() {

    socket.on('new', newstate => {
      this.props.newTransaction(newstate)
    })

  }


  render() {

    const { transactions , themes} = this.props


    return (
      <div className='main'>
        <TransactionsFeed transactions={transactions} />
        <Themes themes={themes} />
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    transactions: state.transactions,
    themes: state.themes
  }
}

function mapDispatchToProps(dispatch) {

  return {
    newTransaction: bindActionCreators(newTransaction, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
