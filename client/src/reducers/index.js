import { combineReducers } from 'redux'

import transactions from './transactions'
import themes from './themes'

export default combineReducers({
  transactions, themes
})
