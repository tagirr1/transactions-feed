const initialState = {}
export default function themes(state = initialState, action) {
  
let themesList = state

if (action.type=='NEW_TRANSACTION'){
  if (action.list.hasOwnProperty('theme_id')) {

    if (themesList.hasOwnProperty(action.list.theme_id)) {
        // theme exist! check the project
      /*{theme:1, projects: [{name: wasd, total: 2$}], total: 5$}*/

      let projectName = action.list.project_name

      if (  themesList[action.list.theme_id].projects.hasOwnProperty(projectName) ) {
          //project exist too
          let cur_project = themesList[action.list.theme_id].projects[projectName]
          cur_project.count = (action.list.amount + cur_project.count)
          // console.log('projects exist')

      } else {
          //new project
          // console.dir(themesList[action.list.theme_id])
          // console.log('%c new projects, old: ', 'background: #222; color: #bada55')
          themesList[action.list.theme_id].projects[projectName] = {
            count:  action.list.amount
          }


      }
      themesList[action.list.theme_id].total+=action.list.amount

    } else {
      //new theme_id
      let project_name = action.list.project_name;
      themesList[action.list.theme_id] = {
        projects: {
          [project_name]: {
              count:  action.list.amount
            }
        },
        total: action.list.amount
      }


    }

  } return Object.assign({}, themesList)
  } else {
    return state
  }


}
